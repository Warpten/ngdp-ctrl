﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NGDP.Patch;

namespace NGDP.Utilities
{
    public static class LinkBuilder
    {
        public static string BuildData(CDNs.Record host, byte[] hash) =>
            $"http://{host.Hosts[0]}/{host.Path}/data/{hash[0]:x2}/{hash[1]:x2}/{hash.ToHexString()}";

        public static string BuildConfig(CDNs.Record host, byte[] hash) =>
            $"http://{host.Hosts[0]}/{host.Path}/config/{hash[0]:x2}/{hash[1]:x2}/{hash.ToHexString()}";
    }
}
