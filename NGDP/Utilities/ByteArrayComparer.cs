﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NGDP.Utilities
{
    public class ByteArrayComparer : EqualityComparer<byte[]>
    {
        public override bool Equals(byte[] left, byte[] right)
        {
            if (left == null || right == null)
                return left == right;

            return left.SequenceEqual(right);
        }

        public override int GetHashCode(byte[] key)
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));

            var hash = key[0];
            for (var i = 1; i < key.Length; ++i)
                hash ^= key[i];
            return hash;
        }
    }
}
