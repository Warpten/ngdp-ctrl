﻿using System;
using System.Collections.Generic;
using NGDP.Utilities;

namespace NGDP.NGDP
{
    public class Root
    {
        private Dictionary<ulong, Record> _records { get; } = new Dictionary<ulong, Record>();
        private BiDictionary<int, ulong> FileDataIDXHash = new BiDictionary<int, ulong>();

        public bool TryGetByHash(ulong hash, out Record record) => _records.TryGetValue(hash, out record);

        public bool TryGetByFileDataID(int fileDataID, out Record record)
        {
            ulong hash;
            if (FileDataIDXHash.TryGetByFirst(fileDataID, out hash))
                return TryGetByHash(hash, out record);

            record = default(Record);
            return false;
        }

        public void FromStream(string host, string queryString)
        {
            using (var blte = new BLTE(host))
            {
                blte.Send(queryString);

                var reader = new EndianBinaryReader(EndianBitConverter.Little, blte);

                // This is very gay, but the nature of network streams do not allow anything better.
                try
                {
                    while (true)
                    {
                        var recordCount = reader.ReadInt32();
                        var contentFlags = reader.ReadInt32();
                        var localeFlags = reader.ReadInt32();

                        var records = new Record[recordCount];

                        var fileDataIndex = 0;

                        for (var i = 0; i < recordCount; ++i)
                        {
                            // ReSharper disable once UseObjectOrCollectionInitializer
                            records[i] = new Record();
                            records[i].LocaleFlags = localeFlags;
                            records[i].ContentFlags = contentFlags;

                            records[i].FileDataID = fileDataIndex + reader.ReadInt32();
                            fileDataIndex = records[i].FileDataID + 1;
                        }

                        for (var i = 0; i < recordCount; ++i)
                        {
                            records[i].MD5 = reader.ReadBytes(16);
                            records[i].Hash = reader.ReadUInt64();

                            if (!FileDataIDXHash.ContainsFirst(records[i].FileDataID))
                                FileDataIDXHash.Add(records[i].FileDataID, records[i].Hash);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public class Record
        {
            public byte[] MD5 { get; set; }
            public ulong Hash { get; set; }
            public int FileDataID { get; set; }
            public int ContentFlags { get; set; }
            public int LocaleFlags { get; set; }
        }
    }
}
