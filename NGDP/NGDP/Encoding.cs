﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NGDP.Utilities;

namespace NGDP.NGDP
{
    public class Encoding
    {
        public ushort Flags { get; set; }
        public Dictionary<byte[], Entry> Records = new Dictionary<byte[], Entry>(new ByteArrayComparer());

        public void FromDisk(string fileName)
        {
            using (var reader = File.Open(fileName, FileMode.Open, FileAccess.Read))
                FromStream(reader);
        }

        public void FromNetworkResource(string host, string queryString)
        {
            using (var blte = new BLTE(host))
            {
                blte.Send(queryString);

                FromStream(blte);
            }
        }

        private void FromStream(Stream fileStream)
        {
            using (var reader = new EndianBinaryReader(EndianBitConverter.Little, fileStream))
            {
                if (reader.ReadInt16() != 0x4E45) // EN
                    return;

                reader.ReadByte(); // Unknown
                var checksumSize = reader.ReadByte();
                reader.ReadByte(); // Checksum size in table B, which we won't be using
                Flags = reader.ReadUInt16();
                reader.ReadUInt16(); // Flags for table B, which we won't be using

                reader.BitConverter = EndianBitConverter.Big;
                var tableEntryCount = reader.ReadUInt32();
                reader.ReadUInt32(); // Entry count for table B

                reader.ReadByte(); // Unknown
                var stringBlockSize = reader.ReadInt32(); // String block size (which we won't use)

                // Skip string block and hash headers
                reader.Seek(stringBlockSize, SeekOrigin.Current);
                reader.Seek((int)((16 + 16) * tableEntryCount), SeekOrigin.Current);

                var chunkStart = reader.BaseStream.Position;

                reader.BitConverter = EndianBitConverter.Little;

                for (var i = 0; i < tableEntryCount; ++i)
                {
                    ushort keyCount;
                    while ((keyCount = reader.ReadUInt16()) != 0)
                    {
                        reader.BitConverter = EndianBitConverter.Big;

                        // ReSharper disable once UseObjectOrCollectionInitializer
                        var encoding = new Entry();
                        encoding.Filesize = reader.ReadUInt32();
                        encoding.Hash = reader.ReadBytes(checksumSize);
                        encoding.Key = reader.ReadBytes(checksumSize);

                        reader.Seek(checksumSize * (keyCount - 1), SeekOrigin.Current);
                        Records[encoding.Hash] = encoding;

                        reader.BitConverter = EndianBitConverter.Little;
                    }

                    const int CHUNK_SIZE = 4096;
                    reader.Seek(CHUNK_SIZE - (int)((reader.BaseStream.Position - chunkStart) % CHUNK_SIZE), SeekOrigin.Current);
                }
            }
        }

        public class Entry
        {
            public uint Filesize { get; set; }
            public byte[] Hash { get; set; }
            public byte[] Key { get; set; }
        }
    }
}
