﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NGDP.Network;
using NGDP.Utilities;

namespace NGDP.NGDP
{
    public class IndexStore
    {
        public Dictionary<byte[], Record> Records { get; } = new Dictionary<byte[], Record>(new ByteArrayComparer());

        public void FromStream(string host, string[] archives)
        {
            foreach (var archive in archives)
            {
                using (var client = new AsyncClient(host))
                {
                    var archiveHash = archive.ToByteArray();
                    client.Send($"/tpr/wow/data/{archiveHash[0]:x2}/{archiveHash[1]:x2}/{archive}");

                    using (var reader = new EndianBinaryReader(EndianBitConverter.Big, client.Stream))
                    {
                        var recordCount = client.ContentLength/(16 + 4 + 4);

                        for (var i = 0; i < recordCount; ++i)
                        {
                            // ReSharper disable once UseObjectOrCollectionInitializer
                            var record = new Record();

                            record.Hash = reader.ReadBytes(16);
                            if (!record.Hash.Any(b => b != 0))
                                record.Hash = reader.ReadBytes(16);

                            record.Size = reader.ReadInt32();
                            record.Offset = reader.ReadInt32();
                            record.Archive = archiveHash;
                            Records[record.Hash] = record;
                        }
                    }
                }
            }
        }

        public class Record
        {
            public byte[] Hash { get; set; }
            public byte[] Archive { get; set; }
            public int Size { get; set; }
            public int Offset { get; set; }
        }
    }
}
