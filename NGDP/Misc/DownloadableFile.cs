﻿using System;
using System.IO;
using NGDP.NGDP;
using NGDP.Patch;
using NGDP.Utilities;

namespace NGDP.Misc
{
    public class DownloadableFile
    {
        public string RemoteFilename { get; set; }
        public string LocalFilename { get; set; }

        private static JenkinsHashing Hasher = new JenkinsHashing();

        public event Action<DownloadResult> OnDownloadResult;

        public void Download(Channel channel, CDNs.Record serverInfo)
        {
            using (var fileStream = File.Create(LocalFilename))
            {
                Root.Record rootRecord;
                if (!channel.Root.TryGetByHash(Hasher.ComputeHash(RemoteFilename), out rootRecord))
                {
                    OnDownloadResult?.Invoke(DownloadResult.RootNotFound);
                    return;
                }

                Encoding.Entry encodingRecord;
                if (!channel.Encoding.Records.TryGetValue(rootRecord.MD5, out encodingRecord))
                {
                    OnDownloadResult?.Invoke(DownloadResult.EncodingNotFound);
                    return;
                }

                IndexStore.Record indexEntry;
                if (!channel.Indices.Records.TryGetValue(encodingRecord.Key, out indexEntry))
                {
                    indexEntry = new IndexStore.Record
                    {
                        Offset = 0,
                        Size = -1,
                        Archive = encodingRecord.Hash,
                        Hash = null // Use as a marker for whole-size archives
                    };
                }

                using (var client = new BLTE(serverInfo.Hosts[0]))
                {
                    if (indexEntry.Hash != null)
                        client.AddHeader("Range", $"bytes={indexEntry.Offset}-{indexEntry.Offset + indexEntry.Size - 1}");

                    client.Send($"{serverInfo.Path}/data/{indexEntry.Archive[0]:x2}/{indexEntry.Archive[1]:x2}/{indexEntry.Archive.ToHexString()}");
                    client.CopyTo(fileStream);
                    OnDownloadResult?.Invoke(client.StatusCode == 200 ? DownloadResult.Success : DownloadResult.NetError);
                }
            }
        }
    }

    public enum DownloadResult
    {
        Success,
        RootNotFound,
        EncodingNotFound,
        NetError,
        UnknownError,
    }
}
