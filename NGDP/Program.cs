﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Meebey.SmartIrc4net;

namespace NGDP
{
    internal static class Program
    {
        private static bool _running = true;
        private static IrcClient IRC { get; set; }
        private static List<Channel> Channels = new List<Channel>();

        public static bool SendConfigLinks { get; } = false;

        static void Main(string[] args)
        {
            #region Create channel monitors
            Channels.Clear();
            Channels.Add(new Channel
            {
                ChannelName = "wow",
                DisplayName = "Retail",
                DownloadBinaries = false
            });

            Channels.Add(new Channel
            {
                ChannelName = "wowt",
                DisplayName = "PTR",
                DownloadBinaries = false
            });

            Channels.Add(new Channel
            {
                ChannelName = "wow_beta",
                DisplayName = "Beta",
                DownloadBinaries = false
            });
            #endregion

            foreach (var channel in Channels)
                channel.MessageEvent += (t, c, m) => IRC?.SendMessage(t, c, m);

            Console.CancelKeyPress += (s, ea) =>
            {
                _running = false;
            };

            Connect();

            while (_running)
                Thread.Sleep(10000);// Time to fuck with the CPU
            // ReSharper disable once FunctionNeverReturns
        }

        private static void Connect()
        {
            Task.Factory.StartNew(() =>
            {
                while (_running)
                {
                    foreach (var channel in Channels)
                        channel.Update();
                    Thread.Sleep(10000);
                }
            });
        }
    }
}
