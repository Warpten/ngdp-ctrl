﻿using System.Collections.Generic;

namespace NGDP.Local
{
    public static class BuildManager
    {
        /// <summary>
        /// List of hash codes for known build names.
        /// </summary>
        private static List<string> _knownBuilds = new List<string>();

        public static bool IsBuildKnown(string buildName) => _knownBuilds.Contains(buildName);
        public static void AddBuild(string buildName) => _knownBuilds.Add(buildName);
    }
}
