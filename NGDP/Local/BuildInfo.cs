﻿using System;
using System.Collections.Generic;
using System.IO;
using NGDP.Misc;
using NGDP.Patch;

namespace NGDP.Local
{
    public class BuildInfo
    {
        public string DirectoryName { get; set; }
        private List<DownloadableFile> Files = new List<DownloadableFile>();

        public event Action<DownloadResult, DownloadableFile> OnFileDownloadResult;

        public void AddFile(string remoteFileName, string localFileName)
        {
            Files.Add(new DownloadableFile()
            {
                LocalFilename = localFileName,
                RemoteFilename = remoteFileName
            });
        }

        public void Download(Channel channel, CDNs.Record serverInfo)
        {
            if (!string.IsNullOrEmpty(DirectoryName) && !Directory.Exists(DirectoryName))
                Directory.CreateDirectory(DirectoryName);

            foreach (var file in Files)
            {
                file.LocalFilename = Path.Combine(DirectoryName, file.LocalFilename);
                file.OnDownloadResult += result => OnFileDownloadResult?.Invoke(result, file);
                file.Download(channel, serverInfo);
            }
        }
    }
}
