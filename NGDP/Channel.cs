﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Meebey.SmartIrc4net;
using NGDP.Local;
using NGDP.Misc;
using NGDP.NGDP;
using NGDP.Patch;
using NGDP.Utilities;
using Encoding = NGDP.NGDP.Encoding;

namespace NGDP
{
    public class Channel
    {
        public string ChannelName { get; set; }
        public string DisplayName { get; set; }
        public bool DownloadBinaries { get; set; }

        public Encoding Encoding { get; private set; }
        public Root Root { get; private set; }
        public IndexStore Indices { get; private set; }

        public event Action<SendType, string, string> MessageEvent;

        public void Update()
        {
            using (var versions = new Versions(ChannelName))
            using (var cdns = new CDNs(ChannelName))
            {
                // Walk through versions
                foreach (var versionInfo in versions.Records)
                {
                    Thread.Sleep(500);

                    // Get CDN data.
                    CDNs.Record serverInfo;
                    if (!cdns.Records.TryGetValue(versionInfo.Value.Region, out serverInfo))
                        serverInfo = cdns.Records["eu"];

                    var versionName = versionInfo.Value.GetName(DisplayName);

                    if (BuildManager.IsBuildKnown(versionName))
                        continue;

                    BuildManager.AddBuild(versionName);

                    MessageEvent?.Invoke(SendType.Message, "#trinitycore", $"Build {versionName} was deployed.");

                    // Get build info
                    var buildConfig = new BuildConfiguration(serverInfo.Hosts[0], versionInfo.Value.BuildConfig);
                    var cdnConfig = new ContentConfiguration(serverInfo.Hosts[0], versionInfo.Value.CDNConfig);

                    if (Program.SendConfigLinks)
                    {
                        MessageEvent?.Invoke(SendType.Message, "#trinitycore", $"Build config: {buildConfig.URL}");
                        MessageEvent?.Invoke(SendType.Message, "#trinitycore", $"CDN Config: {cdnConfig.URL}");
                    }

                    if (!DownloadBinaries)
                        continue;

                    Task.Factory.StartNew(() =>
                    {
                        // Get encoding file
                        Encoding = new Encoding();
                        Encoding.FromNetworkResource(serverInfo.Hosts[0],
                            $"/{serverInfo.Path}/data/{buildConfig.Encoding[1][0]:x2}/{buildConfig.Encoding[1][1]:x2}/{buildConfig.Encoding[1].ToHexString()}");

                        // Get root file
                        Encoding.Entry rootEncodingEntry;
                        if (!Encoding.Records.TryGetValue(buildConfig.Root, out rootEncodingEntry))
                            return;

                        Root = new Root();
                        Root.FromStream(serverInfo.Hosts[0],
                            $"/{serverInfo.Path}/data/{rootEncodingEntry.Key[0]:x2}/{rootEncodingEntry.Key[1]:x2}/{rootEncodingEntry.Key.ToHexString()}");

                        // Get all index files
                        Indices = new IndexStore();
                        Indices.FromStream(serverInfo.Hosts[0], cdnConfig.Archives);

                        buildConfig.Dispose();
                        cdnConfig.Dispose();

                        var buildInfo = new BuildInfo {
                            DirectoryName = versionName
                        };
                        buildInfo.AddFile("Wow.exe", "Wow.exe");
                        buildInfo.AddFile("Wow-64.exe", "Wow-64.exe");
                        buildInfo.AddFile("WowT.exe", "WowT.exe");
                        buildInfo.AddFile("WowT-64.exe", "WowT-64.exe");
                        buildInfo.AddFile("WowB.exe", "WowB.exe");
                        buildInfo.AddFile("WowB-64.exe", "WowB-64.exe");
                        buildInfo.AddFile("WowGM.exe", "WowGM.exe");
                        buildInfo.AddFile("WowGM-64.exe", "WowGM-64.exe");
                        buildInfo.AddFile(
                            @"World of Warcraft.app\Contents\MacOS\World of Warcraft.dSYM\Contents\Resources\DWARF\World of Warcraft",
                            "World of Warcraft");

                        buildInfo.OnFileDownloadResult += (result, file) =>
                        {
                            if (result != DownloadResult.Success)
                                return;

                            MessageEvent?.Invoke(SendType.Message,
                                "#trinitycore",
                                $"{file.LocalFilename.Split('\\').Last()} downloaded for build {buildInfo.DirectoryName}.");
                        };
                        buildInfo.Download(this, serverInfo);
                    });
                }
            }

            // Sleep one second to make sure every message goes through.
            Thread.Sleep(500);
        }
    }
}
